# Zadanie

- stwórz aplikację za pomocą create-react-app
- aplikacja ma za zadanie wyświetlać listę zadań pobraną z backendu
- aplikacja ma za zadanie umożliwiać dodanie nowego zadania i wyświetlanie go na liście
- NIE jest potrzebne żadne logowanie, autoryzacja czy router

# Wymagania

- wykorzystaj bibliotekę http://material-ui.com do nadania wyglądu
- wykorzystaj bibliotekę https://github.com/mbrn/material-table do wyświetlania listy w postaci tabelki
- wykorzystaj bibliotekę `@feathersjs/client` do pobierania danych z backendu poprzez REST API - https://docs.feathersjs.com/api/client/rest.html
- wykorzystaj bibliotekę https://github.com/feathers-plus/feathers-redux do automatycznego stworzenia reducerów i akcji reduxowych do pobierania danych z backendu (UWAGA! Musisz mieć skonfigurowanego store z middlewarami `redux-promise-middleware` i `redux-thunk`)
- dane z reduxa połącz z komponentem poprzez `connect`


# Backend

Backend stworzony jest w [Feathers](http://feathersjs.com). 

## Włączenie serwisu backendowego


1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/feathers-demo; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Działanie serwisu backendowego

Pod adresem `http://localhost:3030` jest dostępny serwis backendowy. Posiada on jeden edpoint REST-owy - `/todos`. Endpoint obsługuje wszystkie podstawowe REST-owe zapytania (tu przykład z dokumentacji https://docs.feathersjs.com/guides/basics/rest.html#rest-and-services).

Możesz za pomoca Postmana / Advenced REST Client, sprawdzić jego działanie wysyłając zapytania GET i POST (nie będziesz potrzebował pozostałych, by spełnić wymagania zadania :)) na powyższy adres.

Serwis NIE wymaga żadnej autoryzacji. Dane zapisuje w plikach na dysku, nie wymaga więc również istnienia żadnej bazy danych. `npm start` i masz backend.

Pamiętaj, że w aplikacji chcemy użyć biblioteki klienckiej `@feathersjs/client` i jej integracji z reduxem `feathers-redux`, a NIE `fetch` i bezpośredniego odpytywania adresu URL. Możesz sobie to swobodnie prównać do możliwości Firebase, dało się przez `fetch` i przez bibliotekę `firebase`.

Jeśli napotkasz problem, ze zrozumieniem jak działa Feathers możemy się zdzwonić.

# POWODZENIA :)
